const os = require('os');
const axios = require('axios');
const Settings = require('./settings');
const GUID = require('./guid');
const {ipcRenderer} = require('electron');

const guid = new GUID();
const platformUUID = guid.getPlatformUUID();
const osPlatform = os.platform();

const platformData = async () => {
    return {"uuid": await platformUUID, "platform": osPlatform};
}

module.exports = class API {
    constructor() {
        this.settings = new Settings();
    }

    async fetchAuth(email, password) {
        const data = {email: email, password: password};
        const headers = {"Content-Type": "application/json", "platform": await platformData()};
        const url = this.settings.server_url + this.settings.auth;
        const resp = await axios.post(url, data, {headers: headers})
            .then(response => {
                //response["refresh_token"] = r.cookies.get("refreshToken")
                console.log('auth response: ', response)
                console.log('response.headers', response.headers)
                return response;
            })
            .catch(error => {
                console.log('[fetchAuth] Api error ', error);
                return null;
            });
        console.log('resp.headers ', resp.headers)

        return resp;
    }

    async checkUserAuthenticated(accessToken, refreshToken) {
        const url = this.settings.server_url + this.settings.check_user_authenticated;
        const config = {
            headers: {
                "Content-Type": "application/json",
                "Authorization": accessToken,
                "platform": await platformData(),
                // "Cookie": "refreshToken=" + refreshToken + ";"
            },
            withCredentials: true
        }
        const resp = await axios.get(url, config)
            .then(response => {
                return response;
            })
            .catch(error => {
                console.log('[checkUserAuthenticated] Api error ', error);
                return null;
            });
        return resp;
    }

    async fetchProjects(accessToken, refreshToken) {
        const url = this.settings.server_url + this.settings.get_projects;
        const config = {
            headers: {
                "Content-Type": "application/json",
                "Authorization": accessToken,
                "platform": await platformData(),
                // "Cookie": "refreshToken=" + refreshToken + ";"
            },
            withCredentials: true
        }
        const resp = await axios.get(url, config)
            .then(response => {
                return response;
            })
            .catch(error => {
                console.log('[fetchProjects] Api error ', error);
                return null;
            });
        return resp;
    }

    async toggleProject(accessToken, refreshToken, projectId) {
        const url = this.settings.server_url + this.settings.project_toggle + '?projectId=' + projectId;
        const config = {
            headers: {
                "Content-Type": "application/json",
                "Authorization": accessToken,
                "platform": await platformData(),
                // "Cookie": "refreshToken=" + refreshToken + ";"
            },
            withCredentials: true
        }
        const resp = await axios.get(url, config)
            .then(response => {
                return response;
            })
            .catch(error => {
                console.log('[toggleProject] Api error ', error);
                return null;
            });
        return resp;
    }

    async workActivity(accessToken, refreshToken) {
        const url = this.settings.server_url + this.settings.work_activity;
        const config = {
            headers: {
                "Content-Type": "application/json",
                "Authorization": accessToken,
                "platform": await platformData(),
                // "Cookie": "refreshToken=" + refreshToken + ";"
            },
            withCredentials: true
        }
        const resp = await axios.get(url, config)
            .then(response => {
                return response;
            })
            .catch(error => {
                console.log('[workActivity] Api error ', error);
                return null;
            });
        return resp;
    }

    async sendScreenshot(accessToken, refreshToken, filePath, fileName, projectId) {
        ipcRenderer.send('screenshot:save', {
                filePath: filePath,
                accessToken: accessToken,
                projectId: projectId,
            }
        );
    }

    async sendIntensity(accessToken, refreshToken, intensity, projectId) {
        const data = {'intensity': intensity};
        const url = this.settings.server_url + this.settings.intensity + '/' + projectId;
        const config = {
            headers: {
                "Content-Type": "application/json",
                "Authorization": accessToken,
                "platform": await platformData(),
                // "Cookie": "refreshToken=" + refreshToken + ";"
            },
            withCredentials: true
        }
        const resp = await axios.post(url, data, config)
            .then(response => {
                return response;
            })
            .catch(error => {
                console.log('[sendIntensity] Api error ', error);
                return null;
            });
        return resp;
    }
}