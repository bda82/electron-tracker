const Settings = require('./settings');
var jwt = require('jwt-simple');

module.exports = class JWT {
    constructor() {
        this.settings = new Settings();
    }
    encodeJWT(payload) {
        return jwt.encode(payload, this.settings.jwtSecret);
    }
    decodeJWT(token) {
        return jwt.decode(token, this.settings.jwtSecret);
    }
}