const Record = require('./record');

module.exports = class Project {
    constructor(id, name, repositoryUrl, record) {
        this.id = id;
        this.name = name;
        this.repositoryUrl = repositoryUrl;
        this.record = record;
    }
}