const {ipcRenderer} = require('electron');
const MainObject = require('./main_object');
const State = require('./state');
const Record = require('./record');

let mainObject = new MainObject();
let state = new State();
let mainTimerObject = null;
let keyboardTimerObject = null;
let screenshotTimerObject = null;
let idleTimerObject = null;

const changeTimeDisplay = () => {
    if (mainObject.currentProject.record != null) {
        let trackedTime = mainObject.millisecondsToTime(mainObject.currentProject.record.milliseconds);
        const trackedHours = trackedTime.hours < 10 ? '0' + trackedTime.hours : trackedTime.hours;
        const trackedMinutes = trackedTime.minutes < 10 ? '0' + trackedTime.minutes : trackedTime.minutes;
        const trackedSeconds = trackedTime.seconds < 10 ? '0' + trackedTime.seconds : trackedTime.seconds;
        document.getElementById('trackedTime').innerText = trackedHours + ':' + trackedMinutes + ':' + trackedSeconds;
    } else {
        document.getElementById('trackedTime').innerText = '00:00:00';
    }
}
const fillProjectsSelector = (projects) => {
    projects.forEach(projectData => {
        let option = document.createElement("option");
        option.value = projectData.id;
        option.text = projectData.name;
        document.getElementById('projectSelect').add(option);
    });
}
const fillCurrentProjectName = (projectName) => {
    document.getElementById('currentProjectName').innerText = 'Projects (' + projectName + '):'
}
const fillScreenshot = (imageSrc) => {
    document.getElementById('placeholder').src = imageSrc;
}
const fillIntensity = (intensityValue) => {
    document.getElementById('keyboardCapture').value = intensityValue;
}
const disableStartBtn = () => {
    document.getElementById('startBtn').setAttribute('disabled', '');
}
const enableStartBtn = () => {
    document.getElementById('startBtn').removeAttribute('disabled');
}
const disableStopBtn = () => {
    document.getElementById('stopBtn').setAttribute('disabled', '');
}
const enableStopBtn = () => {
    document.getElementById('stopBtn').removeAttribute('disabled');
}
const disableLoginBtn = () => {
    document.getElementById('loginBtn').setAttribute('disabled', '');
}
const enableLoginBtn = () => {
    document.getElementById('loginBtn').removeAttribute('disabled');
}
const disableProjectSelector = () => {
    document.getElementById('projectSelect').setAttribute('disabled', '');
}
const enableProjectSelector = () => {
    document.getElementById('projectSelect').removeAttribute('disabled');
}
// SENDERS
const sendCommandMakeScreenshot = () => {
    const userId = mainObject.userId == null ? 'undefined' : mainObject.userId;
    ipcRenderer.send('screenshot:capture', {userId: userId});
}
const sendCommandLoadConfig = () => {
    ipcRenderer.send('config:load', {});
}
const sendCommandSaveConfig = (email, password) => {
    const savedData = {email: email, password: password};
    ipcRenderer.send('config:create', savedData);
}
const openLoginModal = () => {
    const modal = document.getElementById('loginModal');
    modal.classList.add('is-active');
}
const closeLoginModal = () => {
    const modal = document.getElementById('loginModal');
    modal.classList.remove('is-active');
}
// Timers
//    Main Timer (1s)
const mainTimerTick = () => {
    if (mainObject.canWork) {
        if (mainObject.currentProject.record != null && mainObject.canWork) {
            mainObject.currentProject.record.milliseconds = mainObject.currentProject.record.milliseconds + 1000;
            changeTimeDisplay();
        }
    }
}
const startMainTimer = () => {
    mainTimerObject = setInterval(mainTimerTick, 1000);
}
const stopMainTimer = () => {
    clearInterval(mainTimerObject);
}
//    Screenshot Timer
const screenshotTimerTick = () => {
    if (mainObject.canWork) sendCommandMakeScreenshot();
}
const startScreenshotTimer = () => {
    screenshotTimerObject = setInterval(screenshotTimerTick, 300000);
}
const stopScreenshotTimer = () => {
    clearInterval(screenshotTimerObject);
}
//    Keyboard Timer
const keyboardTimerTick = async () => {
    if (mainObject.canWork) {
        await mainObject.sendIntensity(mainObject.keyboardCapturer.counter);
        mainObject.keyboardCapturer.append()
        mainObject.keyboardCapturer.clear();
        fillIntensity(0);
    }
}
const startKeyboardTimer = () => {
    keyboardTimerObject = setInterval(keyboardTimerTick, 60000);
}
const stopKeyboardTimer = () => {
    clearInterval(keyboardTimerObject);
}
//    Idle Timer
const idleTimerTick = async () => {
    if (mainObject.canWork) {
        let intervals = 0;
        mainObject.keyboardCapturer.keyCounters.forEach(counter => {
            if (counter !== 0) intervals = intervals + 1;
        })
        if (intervals < 3) {
            alert('You dont work last time! Your Timer will stop now');
            await stateMachine(state.STOP);
        }
    }
}
const startIdleTimer = () => {
    idleTimerObject = setInterval(idleTimerTick, 10 * 60000)
}
const stopIdleTimer = () => {
    clearInterval(idleTimerObject);
}

// State Machine
const stateMachine = async (nextState) => {
    if (nextState === state.WAIT_FOR_START) {
        await makeWaitForStartState();
    } else if (nextState === state.WAIT_FOR_LOGIN) {
        await makeWaitForLoginState();
    } else if (nextState === state.START) {
        await makeStartState();
    } else if (nextState === state.STOP) {
        await makeStopState();
    } else if (nextState === state.WORK) {
        await makeWorkState();
    } else if (nextState === state.ERROR) {
        await makeErrorState();
    }
}
const makeWaitForStartState = async () => {
    // Buttons
    enableStartBtn();
    disableStopBtn();
    disableLoginBtn();
    mainObject.canWork = false;
}
const makeWaitForLoginState = async () => {
    // Buttons
    disableStartBtn();
    disableStopBtn();
    enableLoginBtn();
    mainObject.canWork = false;
}
const makeStartState = async () => {
    // Buttons
    enableStopBtn();
    disableStartBtn();
    disableLoginBtn();
    // Timers
    startScreenshotTimer();
    startMainTimer();
    startKeyboardTimer();
    startIdleTimer();

    disableProjectSelector();

    await mainObject.stopAllProjectsOnServer();

    if (mainObject.currentProject != null) {
        await mainObject.toggleCurrentProject();
    }

    if (mainObject.currentProject.record == null) {
        mainObject.currentProject.record = new Record(
            '',
            true,
            0,
            '',
            ''
        );
    }

    alert('Start project: ' + mainObject.currentProject.name);

    await stateMachine(state.WORK);
}
const makeStopState = async () => {
    // Timers
    stopScreenshotTimer();
    stopMainTimer();
    stopKeyboardTimer();
    stopIdleTimer()

    enableProjectSelector();

    mainObject.keyboardCapturer.clear();
    mainObject.keyboardCapturer.clearCounters();
    fillIntensity(0);

    await mainObject.stopAllProjectsOnServer();

    alert('Stop project: ' + mainObject.currentProject.name);

    await stateMachine(state.WAIT_FOR_START);
}
const makeWorkState = async () => {
    mainObject.canWork = true;
}
const makeErrorState = async () => {
    mainObject.canWork = false;
    disableLoginBtn();
    disableProjectSelector();
    disableStartBtn();
    disableStopBtn();
    stopScreenshotTimer();
    stopMainTimer();
    stopKeyboardTimer();
    stopIdleTimer();
    mainObject.keyboardCapturer.clear();
    mainObject.keyboardCapturer.clearCounters();
    fillIntensity(0);
    alert('Stop project - something is wrong!')
}

window.addEventListener('DOMContentLoaded', async () => {
    mainObject.state = state.WAIT_FOR_LOGIN;
    sendCommandLoadConfig();
    disableStartBtn();
    disableStopBtn();

    // Buttons Event listeners
    document.getElementById('startBtn').addEventListener('click', async () => {
        await stateMachine(state.START);
    });
    document.getElementById('stopBtn').addEventListener('click', async () => {
        await stateMachine(state.STOP);
    });
    document.getElementById('loginBtn').addEventListener('click', async () => {
        openLoginModal();
        await stateMachine(state.WAIT_FOR_START);
    });
    document.getElementById('loginModalCloseBtn').addEventListener('click', () => {
        closeLoginModal();
    })
    // Login Modal
    document.getElementById('loginModalSubmitBtn').addEventListener('click', async () => {
        mainObject.email = document.getElementById('loginEmailField').value;
        mainObject.password = document.getElementById('loginPasswordField').value;
        closeLoginModal();
        sendCommandSaveConfig(mainObject.email, mainObject.password);

        const accessToken = await mainObject.makeAuth();
        if (accessToken != null) {
            // Fetch Projects
            await mainObject.stopAllProjectsOnServer();

            if (mainObject.projects.length > 0) {
                mainObject.currentProject = mainObject.projects[0];
            } else {
                alert('You have not active projects on Server!');
                await stateMachine(state.ERROR);
            }

            // Append projects int selector
            fillProjectsSelector(mainObject.projects);
            // Append current project name
            fillCurrentProjectName(mainObject.currentProject.name);
            // Set time
            changeTimeDisplay();

            fillIntensity(0);

            enableProjectSelector();

            await stateMachine(state.WAIT_FOR_START);
        } else {
            alert('Cant authorize on Server!');
            await stateMachine(state.ERROR);
        }
    });

    // Select Event listener
    document.getElementById("projectSelect").onchange = () => {
        const value = document.getElementById("projectSelect").value;
        mainObject.projects.forEach(projectData => {
            if (value == projectData.id) {
                mainObject.currentProject = projectData;
                document.getElementById('currentProjectName').innerText = 'Projects (' + mainObject.currentProject.name + '):'
            }
        });
        changeTimeDisplay();
    };

    // Make Start screenshot
    sendCommandMakeScreenshot();
    // IPC receivers
    ipcRenderer.on('screenshot:captured', async (error, imageData) => {
        fillScreenshot(imageData.image);
        if (mainObject.canWork) {
            await mainObject.sendScreenshot(imageData.filePath, imageData.fileName)
        }
    });
    ipcRenderer.on('keyboard:captured', (error, counter) => {
        if (mainObject.canWork) {
            mainObject.keyboardCapturer.inc();
            fillIntensity(Math.floor(mainObject.keyboardCapturer.counter / 3));
        }
    });
    ipcRenderer.on('config:loaded', (error, loginData) => {
        mainObject.email = loginData.email;
        mainObject.password = loginData.password;
        if (mainObject.email != null && mainObject.password != null) {
            document.getElementById('loginEmailField').value = loginData.email;
            document.getElementById('loginPasswordField').value = loginData.password;
        } else {
            document.getElementById('loginEmailField').value = '';
            document.getElementById('loginPasswordField').value = '';
        }
    });
});
