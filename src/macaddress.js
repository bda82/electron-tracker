const macaddress = require('macaddress');

module.exports = class MACAddress {
    constructor() {
        this.mac = "11:22:33:aa:bb:cc";
    }

    async get() {
        this.mac = await macaddress.one().then(mac => mac);
        return this.mac;
    }
}