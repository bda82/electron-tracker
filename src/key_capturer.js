module.exports = class KeyboardCapturer {
    constructor() {
        this.clear()
        this.keyCounters = [];
    }
    clear() {
        this.counter = 0
    }
    clearCounters() {
        this.keyCounters = [];
    }
    append() {
        this.keyCounters.push(this.counter);
    }
    inc() {
        this.counter = this.counter + 1;
    }
}