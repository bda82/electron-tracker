module.exports = class Record {
    constructor(beginning, enable, milliseconds, start, stop) {
        this.beginning = beginning;
        this.enable = enable;
        this.milliseconds = milliseconds;
        this.start = start;
        this.stop = stop;
    }
}