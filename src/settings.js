module.exports = class Settings {
    constructor(server_type = 'local') {
        this.auth = "auth/login";
        this.check_user_authenticated = "auth/check";
        this.get_projects = "projects/with_record";
        this.project_toggle = "tracker/toggle";
        this.work_activity = "records/work_activity";
        this.send_screenshot = "projects/screenshot";
        this.intensity = "projects/intensity";
        this.send_processes = "tracker_prod/processes";
        this.refresh_token = "auth/refresh";
        if (server_type === 'dev') this.server_url = "https://timer-new.websailors.pro/api/";
        else if (server_type === 'prod') this.server_url = "https://time.websailors.pro/api/";
        else this.server_url = "http://localhost:3000/api/";
        this.intensityMaximumPerMinute = 30;
        this.jwtSecret = "secret"
        this.jwtAlgorithm = "HS256"
    }
}