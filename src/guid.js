const getUuid = require('uuid-by-string');
const MACAddress = require('./macaddress');

module.exports = class GUID {
    constructor() {
        this.macaddress = new MACAddress();
        this.mac = "11:22:33:aa:bb:cc";
        this.platformUUID = getUuid(this.mac);
    }

    async getPlatformUUID() {
        this.mac = await this.macaddress.get();
        this.platformUUID = getUuid(this.mac);
        return this.platformUUID;
    }
}