const API = require('./api');
const KeyboardCapturer = require('./key_capturer');
const Project = require('./project');
const Record = require('./record');

module.exports = class MainObject {
    constructor() {
        this.canWork = false;
        this.API = new API();
        this.keyboardCapturer = new KeyboardCapturer();
        this.accessToken = null;
        this.refreshToken = null;
        this.currentProject = null;
        this.projects = [];
        this.email = '';
        this.password = '';
        this.userId = null;
    }

    millisecondsToTime(milliseconds) {
        let ms = Math.floor(milliseconds / 1000);
        let time = Math.floor(ms % (24 * 3600));
        let hours = Math.floor(time / 3600);
        time = Math.floor(time % 3600);
        let minutes = Math.floor(time / 60);
        time = Math.floor(time % 60);
        let seconds = time;

        return {
            milliseconds: ms,
            seconds: seconds,
            minutes: minutes,
            hours: hours,
        }
    }

    async makeAuth() {
        const resp = await this.API.fetchAuth(this.email, this.password);
        if (resp === null) {
            console.log('Auth Error!');
            this.accessToken = null;
            this.refreshToken = null;
            this.canWork = false;
        } else {
            if (resp.data.message === 'Success' && resp.data.result != null) {
                const token = resp.data.result.token;
                const accessToken = resp.data.result.accessToken;
                if (token && !accessToken) {
                    this.accessToken = token;
                } else {
                    this.accessToken = accessToken;
                }
                if (resp.data.refresh_token) {
                    this.refreshToken = resp.data.refresh_token;
                } else {
                    this.refreshToken = null;
                }
                const r = await this.API.checkUserAuthenticated(this.accessToken, this.refreshToken)
                    .then(response => {
                        const result = response.data.result;
                        if (result) {
                            this.userId = result.id;
                        }
                    })
                    .catch(error => {
                        console.log('checkUserAuthenticated Error ', error)
                    })
            } else {
                console.log('[makeAuth] Cant parse Auth response data', resp);
                this.accessToken = null;
                this.refreshToken = null;
                this.canWork = false;
            }
        }
        return this.accessToken;
    }

    async makeRefreshToken() {
        // const resp = await this.API.refreshToken(this.accessToken, this.refreshToken)
        await this.API.refreshToken(this.accessToken, this.refreshToken)
        // if (resp === null) {
        //     console.log('Refresh Token Error!');
        //     this.accessToken = null;
        //     this.canWork = false;
        // } else {
        //     const token = resp.data.result.token;
        //     const accessToken = resp.data.result.accessToken;
        //     console.log('token', token);
        //     console.log('accessToken', accessToken);
        //     if (token && !accessToken) {
        //         this.accessToken = token;
        //     } else {
        //         this.accessToken = accessToken;
        //     }
        //     this.refreshToken = resp.refresh_token;
        //     this.canWork = true;
        // }
        return this.accessToken;
    }

    async fetchProjects() {
        const resp = await this.API.fetchProjects(this.accessToken);
        this.projects = [];
        if (resp === null) {
            console.log('Projects Error!');
            return this.projects;
        } else {
            const result = resp.data.result;
            if (resp.data.message === 'Success' && resp.data.result != null) {
                result.forEach(projectData => {
                    let record = null;
                    if (projectData.record != null) {
                        record = new Record(
                            projectData.record.beginning,
                            projectData.record.enable,
                            projectData.record.milliseconds,
                            projectData.record.start,
                            projectData.record.stop
                        );
                    }
                    const project = new Project(
                        projectData.id,
                        projectData.name,
                        projectData.repositoryUrl,
                        record
                    );
                    this.projects.push(project);
                })
            }
        }
        return this.projects;
    }

    async toggleCurrentProject() {
        if (this.currentProject && this.accessToken) {
            const resp = await this.API.toggleProject(this.accessToken, this.refreshToken, this.currentProject.id);
            return resp
        }
        return null;
    }

    async toggleProjectById(projectId) {
        if (this.accessToken) {
            const resp = await this.API.toggleProject(this.accessToken, this.refreshToken, projectId);
            return resp;
        }
        return null;
    }

    async stopAllProjectsOnServer() {
        await this.fetchProjects();

        this.projects.forEach(async project => {
            if (project.record && project.record.enable) {
                await this.toggleProjectById(project.id);
            }
        });
    }

    async sendIntensity() {
        if (this.currentProject && this.accessToken) {
            const resp = await this.API.sendIntensity(this.accessToken, this.refreshToken, this.keyboardCapturer.counter, this.currentProject.id)
                .then(response => {
                    return response;
                })
                .catch(error => {
                    console.log(error);
                })
            return resp;
        }
        return null;
    }

    async sendScreenshot(filePath, fileName) {
        if (this.currentProject && this.accessToken) {
            const resp = await this.API.sendScreenshot(
                this.accessToken,
                this.refreshToken,
                filePath,
                fileName,
                this.currentProject.id,
                this.userId
            )
                .then(response => {
                    return response;
                })
                .catch(error => {
                    console.log(error);
                })
            return resp;
        }
        return null;
    }
}
