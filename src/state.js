module.exports = class State {
    constructor() {
        this.WAIT_FOR_LOGIN = "WAIT_FOR_LOGIN";
        this.WAIT_FOR_START = "WAIT_FOR_START";
        this.STOP = "STOP";
        this.WORK = "WORK";
        this.START = "START";
        this.ERROR = "ERROR";
    }
}