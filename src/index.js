const gkm = require('gkm');
const {app, BrowserWindow, desktopCapturer, ipcMain} = require('electron');
const path = require('path');
const fs = require('fs');
const axios = require('axios');
const FormData = require('form-data');
const Settings = require('./settings');

const settings = new Settings();

if (require('electron-squirrel-startup')) {
    app.quit();
}

const home = app.getPath('home');
const appFolder = home + '/tracker';
const configFileName = 'tracker.json';
const configFilePath = appFolder + '/' + configFileName;

const sendFile = async (filePath, accessToken, projectId) => {
    const form = new FormData();
    form.append('file', fs.createReadStream(filePath));

    const headers = {
        'Authorization': accessToken,
        'Content-Type': `multipart/form-data`
    }

    await axios.post(
        `${settings.server_url}${settings.send_screenshot}/${projectId}`,
        form,
        {headers: headers, withCredentials: true}
    ).then(response => {
        return response;
    }).catch(error => {
        console.log('axios error: ', error);
        return null;
    });
}

const createWindow = () => {
    const mainWindow = new BrowserWindow({
        width: 600,
        height: 680,
        webPreferences: {
            preload: path.join(__dirname, 'preload.js'),
            nodeIntegration: true,
            contextIsolation: false,
        },
    });

    mainWindow.loadFile(path.join(__dirname, 'index.html'));

    // mainWindow.webContents.openDevTools();
    mainWindow.setMenu(null);

    return mainWindow;
};

app.on('ready', () => {
    const mainWindow = createWindow();

    fs.readdir(appFolder, (err, files) => {
        if (err) throw err;

        for (const file of files) {
            if (file.includes('scr_')) {
                fs.unlink(path.join(appFolder, file), (err) => {
                    if (err) throw err;
                });
            }
        }
    });

    gkm.events.on('key.*', key => {
        mainWindow.webContents.send('keyboard:captured', 3);
    });

    ipcMain.on('screenshot:capture', (error, value) => {
        const userId = value.userId;
        desktopCapturer
            .getSources({
                types: ['screen'],
                thumbnailSize: {
                    width: 1920,
                    height: 1080
                }
            })
            .then(async sources => {
                let image = sources[0].thumbnail.toDataURL();
                const dd = new Date();
                const datestring = dd.getFullYear() + "_" + (dd.getMonth() + 1) + "_" + dd.getDate() + "_" + dd.getHours() + "_" + dd.getMinutes();
                const fileName = 'scr_' + datestring + "_" + userId + ".png";
                const filePath = appFolder + '/' + fileName;
                const png = sources[0].thumbnail.toPNG();
                fs.writeFile(
                    filePath,
                    png,
                    "base64",
                    function (err) {
                        if (err) throw err;
                    }
                );
                mainWindow.webContents.send('screenshot:captured', {
                    "image": image,
                    "filePath": filePath,
                    "fileName": fileName
                });
            });
    });

    ipcMain.on('config:load', (error, value) => {
        let loginData = {email: null, password: null};
        if (fs.existsSync(appFolder)) {
            const fileContent = fs.readFileSync(configFilePath);
            const data = JSON.parse(fileContent);
            try {
                loginData.email = data.email;
                loginData.password = data.password;
            } catch {
                //
            }
        }
        mainWindow.webContents.send('config:loaded', loginData);
    });

    ipcMain.on('config:create', (error, value) => {
        if (!fs.existsSync(appFolder)) {
            fs.mkdir(appFolder);
        }
        const data = JSON.stringify(value, null, 2);
        fs.writeFileSync(configFilePath, data);
    });

    ipcMain.on('screenshot:save', async (error, value) => {
        const filePath = value.filePath;
        const accessToken = value.accessToken;
        const projectId = value.projectId;
        const response = await sendFile(filePath, accessToken, projectId);
        mainWindow.webContents.send('screenshot:saved', response);
    });
});

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});
